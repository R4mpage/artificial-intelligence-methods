import random
import time

import keyboard
import pyautogui
import pydirectinput

from config import *


is_running = False


def update_running_state():
    global is_running
    if keyboard.is_pressed(STATE_KEY):
        pyautogui.alert(f"will be {'DIS' if is_running else 'EN'}ABLED", "Bot state")
        is_running = not is_running


def on_event_found(target):
    print("Found event!")
    pyautogui.moveTo(target, duration=EVENT_AIM_DURATION)

    # Zoom close to event
    for _ in range(20):
        pyautogui.scroll(20)

    # Loading to event
    pydirectinput.press("enter")
    time.sleep(0.2)
    pydirectinput.press("enter")
    time.sleep(20)

    for _ in range(WAIT_TIME_SEC):
        print("Time left:", WAIT_TIME_SEC - _, "sec.")
        if bool(random.getrandbits(1)):
            pyautogui.move(300, 50, 5)
        if bool(random.getrandbits(1)):
            pyautogui.keyDown("w")
            time.sleep(0.01)
            pyautogui.keyUp("w")
        if bool(random.getrandbits(1)):
            pyautogui.keyDown("a")
            time.sleep(0.01)
            pyautogui.keyUp("a")
        if bool(random.getrandbits(1)):
            pyautogui.keyDown("s")
            time.sleep(0.01)
            pyautogui.keyUp("s")
        if bool(random.getrandbits(1)):
            pyautogui.keyDown("d")
            time.sleep(0.01)
            pyautogui.keyUp("d")

    time.sleep(1)


def find_event(callback=on_event_found) -> bool:
    target = pyautogui.locateOnScreen(CHECK_EVENT_IMG_PATH, confidence=CONFIDENCE)
    if target is not None:
        callback(target)
        return True
    return False


def check_map_open():
    # Scroll to bottom right corner
    for _ in range(3):
        pyautogui.moveTo(
            COORDINATES_TO_CORNERS_LR[0],
            COORDINATES_TO_CORNERS_LR[1],
            MAP_SCROLL_DURATION,
        )
        pyautogui.dragTo(
            COORDINATES_TO_CORNERS_UD[0],
            COORDINATES_TO_CORNERS_UD[1],
            MAP_SCROLL_DURATION,
            button="right",
        )

    # Check for map open
    if pyautogui.locateOnScreen(CHECK_MAP_IMG_PATH, confidence=CONFIDENCE) is not None:
        print("Map is open! Continuing searching...")
    else:
        print("Map is not open, opening...")
        pydirectinput.click()
        time.sleep(0.5)
        pydirectinput.press("esc")
        time.sleep(0.5)
        pydirectinput.press("esc")
        time.sleep(0.5)


def check_for_overweight():
    if (
        pyautogui.locateOnScreen(CHECK_MAP_IMG_OVERWEIGHT_PATH, confidence=CONFIDENCE)
        is not None
    ):
        print("You have over weight. Please, put resources to your repository. Exiting")
        exit()


def start_search_cycle():

    time.sleep(3)

    # Set minimal zoom
    for _ in range(20):
        pyautogui.scroll(-20)

    check_map_open()

    check_for_overweight()

    # Set minimal zoom when player were not in the map on the first time
    for _ in range(20):
        pyautogui.scroll(-20)

    # Scroll to top left corner
    for _ in range(3):
        pyautogui.moveTo(
            COORDINATES_TO_CORNERS_UD[0],
            COORDINATES_TO_CORNERS_UD[1],
            MAP_SCROLL_DURATION,
        )
        pyautogui.dragTo(
            COORDINATES_TO_CORNERS_LR[0],
            COORDINATES_TO_CORNERS_LR[1],
            MAP_SCROLL_DURATION,
            button="middle",
        )

    while True:

        # Check is_running, find event, move to next map position...

        update_running_state()
        if not is_running or find_event():
            break

        pyautogui.moveTo(10, 1000, MAP_SCROLL_DURATION)
        pyautogui.drag(10, -1000, MAP_SCROLL_DURATION, button="middle")

        update_running_state()
        if not is_running or find_event():
            break

        pyautogui.moveTo(10, 1000, MAP_SCROLL_DURATION)
        pyautogui.drag(10, -1000, MAP_SCROLL_DURATION, button="middle")

        update_running_state()
        if not is_running or find_event():
            break

        pyautogui.moveTo(1900, 10, MAP_SCROLL_DURATION)
        pyautogui.drag(-1700, 10, MAP_SCROLL_DURATION, button="middle")

        update_running_state()
        if not is_running or find_event():
            break

        pyautogui.moveTo(10, 10, MAP_SCROLL_DURATION)
        pyautogui.drag(10, 1000, MAP_SCROLL_DURATION, button="middle")

        update_running_state()
        if not is_running or find_event():
            break

        pyautogui.moveTo(10, 10, MAP_SCROLL_DURATION)
        pyautogui.drag(10, 1000, MAP_SCROLL_DURATION, button="middle")

        update_running_state()
        if not is_running or find_event():
            break

        pyautogui.moveTo(10, 1000, MAP_SCROLL_DURATION)
        pyautogui.drag(1900, 10, MAP_SCROLL_DURATION, button="middle")

        update_running_state()
        if not is_running or find_event():
            break


def main():
    print("Press 0 to start/pause:")

    while True:
        update_running_state()

        if is_running:
            start_search_cycle()
            print("2 min sleeping... If you want to exit - press Ctrl+C in console.")
            try:
                time.sleep(60 * 2)
            except KeyboardInterrupt:
                print("Cancel sleeping. Press 0 to start/pause:")
        else:
            time.sleep(0.1)


if __name__ == "__main__":
    main()
