import matplotlib.pyplot as pyplot
import pandas
import pylab
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import Perceptron
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier


data_frame = pandas.read_csv("tennis.csv")
colors = ['#ec1c24','#0ed145']
data_frame["outlook"] = data_frame["outlook"].apply(lambda x: 2 if x == "rainy" else (1 if x == "overcast" else 0))
teach_data_frame = data_frame.iloc[:14]
test_data_frame = data_frame.iloc[14:]


def init(data_frame):
    data_frame = data_frame.copy()
    X = data_frame.drop("play", axis=1)
    y = data_frame["play"]
    X = pandas.DataFrame(X, index=X.index, columns=X.columns)
    return X, y


scaler = StandardScaler()
X_teach, y_teach = init(teach_data_frame)
X_teach = scaler.fit_transform(X_teach)
X_test, y_test = init(test_data_frame)
X_test = scaler.fit_transform(X_test)


perceptron = Perceptron().fit(X_teach, y_teach)
perceptron_predictions = pandas.Series(perceptron.predict(X_test))
print("Точность предсказания методом perceptron: " + str(perceptron.score(X_test, y_test) * 100) + "%")

pylab.figure(figsize=(20, 10))
pylab.subplot(1, 2, 1)
pyplot.pie(y_test.value_counts().sort_index(), labels=sorted(y_test.unique()), autopct="%1.1f%%", shadow=True, colors=colors)
pyplot.title("Реальные данные игр")
pylab.subplot(1, 2, 2)
pyplot.pie(perceptron_predictions.value_counts().sort_index(), labels=sorted(perceptron_predictions.unique()), autopct="%1.1f%%", shadow=True, colors=colors)
pyplot.title("Предсказанные данные игр по perceptron")
pyplot.show()


gaus_nmb = GaussianNB().fit(X_teach, y_teach)
gaus_nmb_predictions = pandas.Series(gaus_nmb.predict(X_test))
print("Точность предсказания методом gaus_nmb: " + str(gaus_nmb.score(X_test, y_test) * 100) + "%")

pylab.figure(figsize=(20, 10))
pylab.subplot(1, 2, 1)
pyplot.pie(y_test.value_counts().sort_index(), labels=sorted(y_test.unique()), autopct="%1.1f%%", shadow=True, colors=colors)
pyplot.title("Реальные данные игр")
pylab.subplot(1, 2, 2)
pyplot.pie(gaus_nmb_predictions.value_counts().sort_index(), labels=sorted(gaus_nmb_predictions.unique()), autopct="%1.1f%%", shadow=True, colors=colors)
pyplot.title("Предсказанные данные игр по gaus_nmb")
pyplot.show()


dik_tree = DecisionTreeClassifier().fit(X_teach, y_teach)
dik_tree_predictions = pandas.Series(dik_tree.predict(X_test))
print("Точность предсказания методом dik_tree: " + str(dik_tree.score(X_test, y_test) * 100) + "%")

pylab.figure(figsize=(20, 10))
pylab.subplot(1, 2, 1)
pyplot.pie(y_test.value_counts().sort_index(), labels=sorted(y_test.unique()), autopct="%1.1f%%", shadow=True, colors=colors)
pyplot.title("Реальные данные игр")
pylab.subplot(1, 2, 2)
pyplot.pie(dik_tree_predictions.value_counts().sort_index(), labels=sorted(dik_tree_predictions.unique()), autopct="%1.1f%%", shadow=True, colors=colors)
pyplot.title("Предсказанные данные игр по SVN")
pyplot.show()
