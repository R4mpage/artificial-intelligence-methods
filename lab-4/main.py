import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import statistics as st
from scipy import stats
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import StandardScaler


k = 7


def euclidean_distance(p1, p2):
    return np.sqrt(np.sum((p1 - p2) ** 2))


def knn(x_couch, y, x_input, k):
    op_labels = []

    for item in test_set_x:
        point_dist = []

        for j in range(len(set_couch_x)):
            distances = euclidean_distance(np.array(set_couch_x[j, :]), item)
            point_dist.append(distances)

        point_dist = np.array(point_dist)

        dist = np.argsort(point_dist)[:k]

        labels = y[dist]
        label = st.mode(labels)

        op_labels.append(label)

    return op_labels


def knn_with_scikit_learn(X, y):
    print('\nKNN с библиотекой scikit-learn')

    x_couch, x_test, y_couch, y_test = train_test_split(
        X, y, test_size=0.3
    )

    scaler = StandardScaler()
    scaler.fit(x_couch)

    x_couch = scaler.transform(x_couch)
    x_test = scaler.transform(x_test)

    model = KNeighborsClassifier(n_neighbors=k)
    model.fit(x_couch, y_couch)

    predictions = model.predict(x_test)

    return x_couch, x_test, y_couch, y_test, predictions

dataset = pd.read_csv('data.csv')

f1 = dataset['Sweetness'].values
f2 = dataset['Crunch'].values

X = np.array(list(zip(f1, f2)), dtype=np.float64)
y = dataset['Product category'].values

set_couch_x = X[:70]
test_set_x = X[70:]

set_couch_y = y[:70]
set_test_y = y[70:]


print('\nЭксперимент номер 1\n')
y_predictions = knn(set_couch_x, set_couch_y, test_set_x, 7)

print('KNN без библиотеки scikit-learn')
print(classification_report(set_test_y, y_predictions))
print(confusion_matrix(set_test_y, y_predictions))

x_couch, x_test, y_couch, y_test, y_pred = knn_with_scikit_learn(X, y)

print('KNN с библиотекой scikit-learn')
print(classification_report(y_test, y_pred))
print(confusion_matrix(y_test, y_pred))

y_show_color = [elem.replace("Fruit", "red", 1)
                .replace("Vegetable", "blue", 1)
                .replace("Protein", "black", 1) for elem in y_predictions]

y_show_color_scikit = [elem.replace("Fruit", "red", 1)
                                  .replace("Vegetable", "blue", 1)
                                  .replace("Protein", "black", 1) for elem in y_pred]

f, ax = plt.subplots(2, 1, figsize=(8, 8))

ax[0].scatter(test_set_x[:, 0], test_set_x[:, 1], c=y_show_color, s=50)
ax[0].set_title('Тест без библиотеки')  

ax[1].scatter(x_test[:, 0], x_test[:, 1], c=y_show_color_scikit, s=50)
ax[1].set_title('Тест с библиотекой')

plt.show()

dataset_new = pd.read_csv('data_new.csv')

f1 = dataset_new['Sweetness'].values
f2 = dataset_new['Crunch'].values

x_new = np.array(list(zip(f1, f2)), dtype=np.float64)
y_new = dataset_new['Product category'].values

set_couch_x = x_new[:84]
test_set_x = x_new[84:]

set_couch_y = y_new[:84]
set_test_y = y_new[84:]

print('\nЭксперимент номер 2\n')

y_predictions_new = knn(set_couch_x, set_couch_y, test_set_x, k)

print('KNN без библиотеки scikit-learn')
print(classification_report(set_test_y, y_predictions_new))
print(confusion_matrix(set_test_y, y_predictions_new))

x_couch_new, x_test_new, y_couch_new, y_test_new, y_show_new = knn_with_scikit_learn(x_new, y_new)

print('KNN с библиотекой scikit-learn')
print(classification_report(y_test_new, y_show_new))
print(confusion_matrix(y_test_new, y_show_new))

y_show_color_new = [elem.replace("Fruit", "red", 1)
                    .replace("Vegetable", "blue", 1)
                    .replace("Protein", "black", 1)
                    .replace("Berry", "green", 1) 
                    for elem in y_predictions_new]

y_show_color_scikit_new = [elem.replace("Fruit", "red", 1)
                                      .replace("Vegetable", "blue", 1)
                                      .replace("Protein", "black", 1)
                                      .replace("Berry", "green", 1) 
                                      for elem in y_show_new]

fig, ax1 = plt.subplots(2, 1, figsize=(8, 8))

ax1[0].scatter(test_set_x[:, 0], test_set_x[:, 1], c=y_show_color_new, s=100)
ax1[0].set_title('Тест без библиотеки')  

ax1[1].scatter(x_test_new[:, 0], x_test_new[:, 1], c=y_show_color_scikit_new, s=100)
ax1[1].set_title('Тест с библиотекой')

plt.show()
